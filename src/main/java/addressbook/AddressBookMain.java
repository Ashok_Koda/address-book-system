package addressbook;

import java.util.ArrayList;
import java.util.Scanner;

public class AddressBookMain {

	private String fName;
	private String lName;
	private String address;
	private String city;
	private int zip;
	private long phoneNumber;
	private String emailId;

	// User input
	public void getUserInput() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the First Name : ");
		this.fName = sc.next();
		System.out.print("Enter the Last Name : ");
		this.lName = sc.next();
		System.out.print("Enter the Address : ");
		this.address = sc.next();
		System.out.print("Enter the City Name : ");
		this.city = sc.next();
		System.out.print("Enter the zip code : ");
		this.zip = sc.nextInt();
		System.out.print("Enter the Phone number : ");
		this.phoneNumber = sc.nextLong();
		System.out.print("Enter the Email ID : ");
		this.emailId = sc.next();
	}

	public void showAddressBook() {
		System.out.println("------------------------------------------------");
		System.out.println("First Name : " + fName);
		System.out.println("Last Name : " + lName);
		System.out.println("Address : " + address);
		System.out.println("City Name : " + city);
		System.out.println("Zip code : " + zip);
		System.out.println("Phone Number : " + phoneNumber);
		System.out.println("Email id : " + emailId);
		System.out.println("------------------------------------------------");
	}

	public static void main(String[] args) {

		System.out.println("------------Welcome To Address Book----------------");

		ArrayList<AddressBookMain> addressList = new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		AddressBookMain addressBook = new AddressBookMain();
		int choose = 1;
		while (choose == 1) {
			addressBook.getUserInput();
			addressList.add(addressBook);

			System.out.print("You want to add more address press 1 or 0 for exit: ");
			choose = sc.nextInt();
			if (choose == 0)
				break;
		}

		for (int i = 0; i < addressList.size(); i++) {
			showAllRecords(addressList.get(i));
		}

	}

	private static void showAllRecords(AddressBookMain addressBookMain) {
		addressBookMain.showAddressBook();
	}

}
