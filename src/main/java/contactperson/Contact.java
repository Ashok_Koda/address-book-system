package contactperson;

import java.util.ArrayList;
import java.util.Scanner;

public class Contact {

	private String fName;
	private String lName;
	private String address;
	private String city;
	private int zip;
	private long phoneno;
	private String emailId;

	public Contact() {

	}

	public Contact getInput() {
		Contact contact = new Contact();
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the First Name : ");
		contact.fName = sc.next();
		System.out.print("Enter the Last Name : ");
		contact.lName = sc.next();
		System.out.print("Enter the Address : ");
		contact.address = sc.next();
		System.out.print("Enter the City Name : ");
		contact.city = sc.next();
		System.out.print("Enter the zip code : ");
		contact.zip = sc.nextInt();
		System.out.print("Enter the Phone number : ");
		contact.phoneno = sc.nextLong();
		System.out.print("Enter the Email ID : ");
		contact.emailId = sc.next();
		return contact;
	}

	public void updateContact(String name, ArrayList<Contact> contactList) {

		for (int i = 0; i < contactList.size(); i++) {
			if (name.equals(contactList.get(i).fName)) {
				contactList.remove(i);
				contactList.add(i, getInput());
			}
		}

	}

	public void deleteContact(String name, ArrayList<Contact> contactList) {

		if (contactList.size() > 0) {
			for (int i = 0; i < contactList.size(); i++) {
				if (name.equals(contactList.get(i).fName)) {
					contactList.remove(i);
				} else
					System.out.println("Person not found with this name " + name);
			}
		} else
			System.out.println("Person not found to delete");
		System.out.println("Record Delete Successfully.......");

	}

	public void addressBook(ArrayList<Contact> contact) {
		for (int j = 0; j < contact.size(); j++) {
			System.out.println("------------------------------------------------");
			System.out.println("First Name : " + contact.get(j).fName);
			System.out.println("Last Name : " + contact.get(j).lName);
			System.out.println("Address : " + contact.get(j).address);
			System.out.println("City Name : " + contact.get(j).city);
			System.out.println("Zip code : " + contact.get(j).zip);
			System.out.println("Phone Number : " + contact.get(j).phoneno);
			System.out.println("Email id : " + contact.get(j).emailId);
			System.out.println("------------------------------------------------");
		}
	}

}
