package contactperson;

import java.util.ArrayList;
import java.util.Scanner;

public class ContactMain {

	public static void main(String[] args) {

		System.out.println("------------Welcome To Contact Book----------------");

		Contact contact = new Contact();

		ArrayList<Contact> contactList = new ArrayList<>();
		String name;
		Scanner sc = new Scanner(System.in);
		boolean flag = true;
		int choose = 1;

		while (flag) {
			System.out
					.print("1 - Add more contact \n2 - Edit Contact \n3 - Delete person Contact \n4 - Show AddressBook "
							+ "\n0 -  for exit \nEnter your option: ");

			choose = sc.nextInt();

			switch (choose) {
			case 1:
				contactList.add(contact.getInput());
				break;
			case 2:
				System.out.println("Enter first name that you want to edit record");
				name = sc.next();
				contact.updateContact(name, contactList);
				break;
			case 3:
				System.out.println("Enter first name that you want to Delete record");
				name = sc.next();
				contact.deleteContact(name, contactList);
				break;
			 case 4:
                 System.out.println("Side of record : " + contactList.size());
                 contact.addressBook(contactList);
                 break;
			 case 0:
                 flag = false;
                 break;
			}
		}

	}

}
